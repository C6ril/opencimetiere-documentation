.. _operation:

#########
Opération
#########

Nous vous proposons dans ce chapitre d'utiliser les opérations.


Les opérations ont été implémentées par la version 2.02
d'openCimetière à l'initiative de la ville d'Albi.

Ce module est facultatif et les opérations funéraires peuvent être
saisies dans les emplacements directement (surtout lorsqu on est en phase
d'initialisation du projet et de saisie en masse).

Elles concernent :

- l'inhumation

- la réduction d'un ou plusieurs défunts

- le transfert d'un ou plusieurs défunt d'un emplacement à un autre

Les transferts ont intégré ce module avec la version 3.0.0.

Les opérations peuvent avoir 2 états :

- actif

- trt (opération traitée)



Lorsqu'elle concerne un défunt d'un emplacement, il est alors impossible
de modifier un défunt lorsqu'une opération est dans l'état actif.

Pour traiter une opération ou la valider, il faut appuyer sur
le bouton "v" de l'opération considérée.

Il est alors lancé le traitement de validation (app/valid_operation.php)
Les tables défunt, emplacement sont alors mises à jour et l'accès au defunt
via l'onglet de l'emplacement est permis

ATTENTION, CE TRAITEMENT EST DEFINITIF et on ne peut pas retourner en arrière.

Les opérations "traitées" sont visualisables dans l'onglet "opération-trt" de
l'emplacement. En appuyant sur l'icone pdf, on accéde à la liste des courriers automatiques
a générer pour l'opération (si elles sont actives).

.. contents::

.. _inhumation:

*********************
Saisir une inhumation
*********************

Il est proposé de décrire dans ce paragraphe la saisie d'une inhumation
dans le module operation

Choisir l'option :

- inhumation concession

- inhumation colombarium

- inhumation terrain communal

- inhumation enfeu


.. image:: a_operation-inhumation-listing.png

Il est possible de creer ou modifier une opération dans le formulaire ci dessous

.. image:: a_operation-inhumation-formulaire-ajout.png



Il est saisie :

- la date et heure de l'opération

- l'emplacement concerné

- l'entreprise effectuant letravail

- le nom du défunt (et autres renseignements)




.. _reduction:

********************
Saisir une reduction
********************



Il est proposé de décrire dans ce paragraphe de decrire la saisie d'une reduction
dans le module operation

Choisir l'option :

- reduction concession

- reduction enfeu 

.. image:: a_operation-reduction-listing.png


Il est possible de creer ou modifier une opération dans le formulaire ci dessous

.. image:: a_operation-reduction-formulaire-ajout.png




Il est saisie :

- la date et heure de l'opération

- l'emplacement concerné

- l'entreprise effectuant le travail


Les défunts concernés sont saisis dans l'onglet operation_defunt

Ils doivent appartenir à l'emplacement considérée.

.. image:: a_operation-reduction-onglet-defunt-listing.png

Mise à jour d'un defunt

.. image:: a_operation-reduction-onglet-defunt-formulaire-ajout.png


Le defunt est alors verrouillé dans l'emplacement

.. image:: a_operation-reduction-concession-onglet-defunt-listing-verrou.png


.. _transfert:

*******************
Saisir un transfert
*******************



Il est proposé de décrire dans ce paragraphe la saisie d'un transfert
dans le module operation

Choisir l'option transfert emplacement

.. image:: a_operation-transfert-listing.png


Il est possible de creer ou modifier l'opération dans le formulaire ci dessous

.. image:: a_operation-transfert-formulaire-ajout.png




Il est saisie :

- la date et heure de l'opération

- l'emplacement concerné

- l'entreprise effectuant le travail

- l'emplacement de transfert


Les défunts concernés sont saisis dans l'onglet operation_defunt

Ils doivent appartenir à l'emplacement de départ.

.. image:: a_operation-transfert-onglet-defunt-listing.png

Mise à jour d'un defunt

.. image:: a_operation-transfert-onglet-defunt-formulaire-ajout.png


Le defunt est alors verrouillé dans l'emplacement

.. image:: a_operation-transfert-concession-onglet-defunt-listing-verrou.png


.. _calcul_place_occupee:

**************************
Calcul de la place occupée
**************************

Ce traitement recalcule la place occupée sous la base des
paramètres de om_parametre (:ref:`paramétrage général <taille_cercueil>`)

Ce traitement est interessant :

- suite a un import en masse de données (par script d import)

- suite a une modification des paramètres de calcul


.. _numdossier:

**********************************
remise à zéro du numéro de dossier 
**********************************

Ce traitement permet de remettre à 0 la numérotation des dossiers
dans la saisie des opérations.

Il est à faire en début d'année.

.. image:: a_operation-traitement-reinitialisation-seq-numdossier.png

