.. _parametrage:

############################
Administration & Paramétrage
############################


Une section dédiée à l’administration et au pamétrage permet de configurer l’application : saisir les données des tables de références, gérer les utilisateurs, paramétrer les éditions PDF, … Pour y accéder, les rubriques 'Administration' et 'Paramétrage' permettent d'accéder aux différents écrans.

.. image:: a_administration-menu-administration.png

.. image:: a_administration-menu-parametrage.png


Dans ce paragraphe il est décrit les principaux éléments pour le manuel utilisateur, si un élément n’est pas décrit ici, il faut se référer au guide du développeur du framework openMairie : https://docs.openmairie.org/?project=framework&version=4.9&format=html&path=usage/administration


.. toctree::

    tables_de_reference.rst
    tables_de_localisation.rst
    geolocalisation.rst
    parametres_generaux.rst

